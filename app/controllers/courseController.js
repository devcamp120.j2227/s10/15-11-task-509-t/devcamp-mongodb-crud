// Khai báo thư viện Mongoose
const mongoose = require("mongoose");

// Khai báo model app
const reviewModel = require("../models/reviewModel");
const courseModel = require("../models/courseModel");


// Get all courses
const getAllCourses = (request, response) => {
    //B1: thu thập dữ liệu (bỏ qua)
    //B2: kiểm tra dữ liệu (bỏ qua)
    //B3: thực hiện load all course
    courseModel.find((error, data) => {
        if (error) {
            return response.status(500).json({
                "status":"Error 500: internal server error",
                "message":error.message
            })
        } else {
            return response.status(201).json({
                "status":"Loadd all courses successfully!",
                "data":data
            })
        }
    })
}

// Create a course
const createCourse =  (request, response) => {
    //B1: thu thập dữ liệu
    let bodyCourse = request.body;
    console.log(bodyCourse);

    //B2: kiểm tra dữ liệu
    // Kiểm tra title
    if (!bodyCourse.title) {
        return response.status(400).json({
            "status":"Error 400: bad request",
            "message":"Title is not valid!"
        });
    }

    // Kiểm tra noStudent
    if (!bodyCourse.noStudent || isNaN(bodyCourse.noStudent) || bodyCourse.noStudent < 0) {
        return response.status(400).json({
            "status":"Error 400: bad request",
            "message":"Number of Student not valid!"
        });
    }

    //B3: thực hiện tạo mới course
    let newCourse = {
        _id:mongoose.Types.ObjectId(),
        title:bodyCourse.title,
        description:bodyCourse.description,
        noStudent:bodyCourse.noStudent
    }

    courseModel.create(newCourse, (error, data) => {
        if (error) {
            return response.status(500).json({
                "status":"Error 500: internal server error",
                "message":error.message
            })
        } else {
            return response.status(201).json({
                "status":"Create new course successfully!",
                "data":data
            })
        }
    })
}


// Get a course by Id
const getCourseById = (request, response) => {
    //B1: thu thập dữ liệu
    const courseId = request.params.courseId;

    //B2: kiểm tra dữ liệu
    // Kiểm tra course id
    if (!mongoose.Types.ObjectId.isValid(courseId)) {
        return response.status(400).json({
            "status":"Error 400: bad request",
            "message":"Course Id is not valid!"
        });
    }

    //B3: thực hiện load course theo id
    courseModel.findById(courseId, (error, data) => {
        if (error) {
            return response.status(500).json({
                "status":"Error 500: internal server error",
                "message":error.message
            })
        } else {
            return response.status(201).json({
                "status":"Get course by id successfully!",
                "data":data
            })
        }
    })
}

// Update course
const updateCourse = (request, response) => {
    //B1: thu thập dữ liệu
    const courseId = request.params.courseId;
    let bodyCourse = request.body;
    console.log(courseId);
    console.log(bodyCourse);

    //B2: kiểm tra dữ liệu
    // Kiểm tra course id
    if (!mongoose.Types.ObjectId.isValid(courseId)) {
        return response.status(400).json({
            "status":"Error 400: bad request",
            "message":"Course Id is not valid!"
        });
    }

    // Kiểm tra title
    if (!bodyCourse.title) {
        return response.status(400).json({
            "status":"Error 400: bad request",
            "message":"Title is not valid!"
        });
    }

    // Kiểm tra noStudent
    if (!bodyCourse.noStudent || isNaN(bodyCourse.noStudent) || bodyCourse.noStudent < 0) {
        return response.status(400).json({
            "status":"Error 400: bad request",
            "message":"Number of Student not valid!"
        });
    }

    //B3: Thực hiện update course theo id
    let newCourse = {
        title:bodyCourse.title,
        description:bodyCourse.description,
        noStudent:bodyCourse.noStudent
    }
    courseModel.findByIdAndUpdate(courseId, newCourse, (error, data) => {
        if (error) {
            return response.status(500).json({
                "status":"Error 500: internal server error",
                "message":error.message
            })
        } else {
            return response.status(201).json({
                "status":"Update course by id successfully!",
                "data":data
            })
        }
    })
}

// Delete course by Id
const deleteCourseById = (request, response) => {
    //B1: thu thập dữ liệu
    const courseId = request.params.courseId;

    //B2: kiểm tra dữ liệu
    // Kiểm tra course id
    if (!mongoose.Types.ObjectId.isValid(courseId)) {
        return response.status(400).json({
            "status":"Error 400: bad request",
            "message":"Course Id is not valid!"
        });
    }


    //B3: Thực hiện xóa course theo id
    courseModel.findByIdAndDelete(courseId, (error, data) => {
        if (error) {
            return response.status(500).json({
                "status":"Error 500: internal server error",
                "message":error.message
            })
        } else {
            return response.status(201).json({
                "status":"Delete course by id successfully!",
                "data":data
            })
        }
    })
}

module.exports = {
    getAllCourses,
    createCourse,
    getCourseById,
    updateCourse,
    deleteCourseById
}